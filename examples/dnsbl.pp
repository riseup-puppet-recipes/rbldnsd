# dnsrbl.pp
# This is an example puppet class for how to use the rbldnsd module to
# implement a DNSBL (in this case using the spamhaus data feed)
#
class dnsbl {

  # override some default rbldnsd module variables
  $rbldnsd_ip = '192.168.0.3'
  $rbldnsd_zones = 'sbl.dnsbl:ip4set:sbl \
                    pbl.dnsbl:ip4trie:pbl \
                    xbl.dnsbl:ip4tset:xbl \
                    zen.dnsbl:ip4set:sbl \
                    zen.dnsbl:ip4trie:pbl \
                    zen.dnsbl:ip4tset:xbl'
  $rbldnsd_workingdir = dnsbl
  $rbldnsd_acl = 'acl'

  include rbldnsd

  # rsync is needed by the spamhaus-sync.sh script
  if !defined(Package['rsync']) {
    package { 'rsync':
      ensure => installed;
    }
  }

  # setup directories
  file {
    '/var/lib/rbldns/spamhaus':
      ensure => directory,
      mode   => '0755',
      owner  => sys,
      group  => root;

    '/var/lib/rbldns/spamhaus-sync.sh':
      source  => "${::fileserver}/dnsbl/spamhaus-sync.sh",
      mode    => '0755',
      owner   => root,
      group   => root,
      require => File['/var/lib/rbldns'];

    # setup symlinks so rbldnsd can see the zones, need to be relative
    # since rbldnsd runs chrooted
    "/var/lib/rbldns/${rbldnsd_workingdir}/pbl":
      ensure  => '../spamhaus/pbl',
      require => File["/var/lib/rbldns/${rbldnsd_workingdir}"];

    "/var/lib/rbldns/${rbldnsd_workingdir}/sbl":
      ensure  => '../spamhaus/sbl',
      require => File["/var/lib/rbldns/${rbldnsd_workingdir}"];

    "/var/lib/rbldns/${rbldnsd_workingdir}/xbl":
      ensure  => '../spamhaus/xbl',
      require => File["/var/lib/rbldns/${rbldnsd_workingdir}"];

    "/var/lib/rbldns/${rbldnsd_workingdir}/acl":
      owner   => root,
      group   => root,
      mode    => '0644',
      source  => "${::fileserver}/dnsbl/acl",
      require => File['/var/lib/rbldns'];
  }

  cron {
    'spamhaus_sync':
      command => '/var/lib/rbldns/spamhaus-sync.sh',
      user    => sys,
      # these times come from spamhaus and are specific to riseup
      minute  => [ 1,31 ];
  }

  # the munin plugin is provided by the rbldnsd module, but we need to
  # define here what we want it to graph
  munin::plugin {
    'rbldnsd-zone_zen.dnsbl':
      ensure      => 'rbldnsd-zone_',
      config      => "user rbldns\nenv.statsdir /var/lib/rbldns/${rbldnsd_workingdir}\nenv.statsfile stats.log\nenv.pidfile /var/run/rbldnsd-dnsbl.pid",
      script_path => '/usr/local/share/munin-plugins';
  }
}
