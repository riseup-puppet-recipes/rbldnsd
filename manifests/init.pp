class rbldnsd(
  $rbldnsd_ip = '127.0.0.1',
  $rbldnsd_root = '/var/lib/rbldns',
  $rbldnsd_workingdir = dnsbl,
  $rbldnsd_log = '',
  $rbldnsd_stats = 'stats.log',
  $rbldnsd_acl = acl,
  $rbldnsd_zones = '',
) {

  package { 'rbldnsd':
    ensure => installed;
  }

  service { 'rbldnsd':
    ensure     => running,
    hasrestart => true,
    hasstatus  => false,
    pattern    => '/usr/sbin/rbldnsd',
    require    => Package['rbldnsd'],
    subscribe  => File['/etc/default/rbldnsd'];
  }

  file {
    '/etc/default/rbldnsd':
      owner   => root,
      group   => root,
      mode    => '0644',
      content => template('rbldnsd/default.erb');

    $rbldnsd_root:
      ensure  => directory,
      mode    => '0755',
      owner   => root,
      group   => root,
      require => Package['rbldnsd'];

    "${rbldnsd_root}/${rbldnsd_workingdir}":
      ensure  => directory,
      mode    => '0755',
      owner   => root,
      group   => root,
      require => File[$rbldnsd_root];

    # plugin from http://www.skullkrusher.net/
    '/usr/local/share/munin-plugins/rbldnsd-zone_':
      source => 'puppet:///modules/rbldnsd/munin/rbldnsd-zone_',
      mode   => '0755',
      owner  => root,
      group  => root;
  }
}
